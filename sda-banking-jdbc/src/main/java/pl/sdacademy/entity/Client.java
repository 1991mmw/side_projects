package pl.sdacademy.entity;

public class Client {
	private Integer id;
	private Integer userId;
	private String name;
	private String surname;
	private Integer addressId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public Integer getAddressId() {
		return addressId;
	}
	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}
	
	@Override
	public String toString() {
		return "Client [id=" + id + ", userId=" + userId + ", name=" + name + ", surname=" + surname + ", addressId="
				+ addressId + "]";
	}
	
	
}
