package pl.sdacademy.repository;

import pl.sdacademy.entity.User;

public interface UserDao {
	public User createUser(User user);
}
