package pl.sdacademy.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import pl.sdacademy.entity.Client;
import pl.sdacademy.repository.ClientDao;

public class ClientDaoImpl implements ClientDao{
	private Connection conn;

	public ClientDaoImpl(Connection conn) {	
		this.conn = conn;
	}

	@Override
	public List<Client> getAllClients() {
		PreparedStatement stmt = null;
		List<Client> result = new LinkedList<>();
		
		try{
			stmt = conn.prepareStatement("select * from client");
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				Client c = new Client();
				
				c.setId(rs.getInt("id"));
				c.setUserId(rs.getInt("user_id"));
				c.setName(rs.getString("name"));
				c.setSurname(rs.getString("surname"));
				c.setAddressId(rs.getInt("address_id"));
				
				result.add(c);
			}	
			return result;
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
		finally{
			try {
				if(stmt != null && !stmt.isClosed()){
					stmt.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Client createClient(Client c) {
		PreparedStatement stmt = null;
				
		try{
			stmt = conn.prepareStatement("insert into client ( id ,user_id, name, surname, address_id) values (0, ? ,? ,?, ?)");
			stmt.setInt(1, c.getUserId());
			stmt.setString(2, c.getName());
			stmt.setString(3, c.getSurname());
			stmt.setInt(4, c.getAddressId());
			
			stmt.executeUpdate();			
			return c;
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
		finally{
			try {
				if(stmt != null && !stmt.isClosed()){
					stmt.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
}
