package pl.sdacademy.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import pl.sdacademy.entity.User;
import pl.sdacademy.repository.UserDao;

public class UserDaoImpl implements UserDao{
	
	private Connection conn;

	public UserDaoImpl(Connection conn) {	
		this.conn = conn;
	}


	@Override
	public User createUser(User user) {
		PreparedStatement stmt = null;
		
		try{
			stmt = conn.prepareStatement("insert into user ( id , password, enabled) values (0, ? ,?)", Statement.RETURN_GENERATED_KEYS);			
			stmt.setString(1, user.getPasword());
			stmt.setBoolean(2, user.isEnabled());			
			
			stmt.executeUpdate();
			
			ResultSet generatedKeyResultSet = stmt.getGeneratedKeys();
			generatedKeyResultSet.next();			
			user.setId(generatedKeyResultSet.getInt(1));
			
			return user;
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
		finally{
			try {
				if(stmt != null && !stmt.isClosed()){
					stmt.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
