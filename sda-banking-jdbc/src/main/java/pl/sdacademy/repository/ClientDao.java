package pl.sdacademy.repository;

import java.sql.Connection;
import java.util.List;

import pl.sdacademy.entity.Client;

public interface ClientDao {
	
	public List<Client> getAllClients();
	
	public Client createClient(Client c);
	
}
