package pl.sdacademy.util.security;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

public class SecurityUtils {
	//TODO SALT
	
	public static String toMd5(String input){
		 try {
		        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
		        byte[] array = md.digest(input.getBytes( "UTF-8" ));
		        StringBuffer sb = new StringBuffer();
		        for (int i = 0; i < array.length; i++) {
		            sb.append( String.format( "%02x", array[i]));
		        }
		        return sb.toString();
		    } catch ( NoSuchAlgorithmException | UnsupportedEncodingException e) {
		        return null;            
		    }

	}	
}
