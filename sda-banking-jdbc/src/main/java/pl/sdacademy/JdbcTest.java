package pl.sdacademy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import pl.sdacademy.entity.Client;
import pl.sdacademy.entity.User;
import pl.sdacademy.repository.ClientDao;
import pl.sdacademy.repository.UserDao;
import pl.sdacademy.repository.impl.ClientDaoImpl;
import pl.sdacademy.repository.impl.UserDaoImpl;

public class JdbcTest {
	public static void main(String[] args) {
		try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            // handle the error
        }
		
		Connection conn = null;
		
		try {
		    conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/systembankowydb", "root", "admin");
		    
		    UserDao userDao = new UserDaoImpl(conn);
		    ClientDao clientDao = new ClientDaoImpl(conn);

		    jdbcCreateUser(userDao);
		    //jdbcCreateClient(clientDao);
		    //jdbcPrintClients(clientDao);
		    
		} catch (SQLException ex) {		    
		    ex.printStackTrace();
		}
		finally {
			try{
				if(conn != null && !conn.isClosed()){			
					conn.close();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private static void jdbcCreateUser(UserDao userDao){
		User newUser = new User();
		newUser.setPasword("1234");
		newUser.setEnabled(true);
		newUser = userDao.createUser(newUser);
		System.out.println("New user created with id: " + newUser.getId());
	}
	
	private static void jdbcCreateClient(ClientDao clientDao){
		   Client newClient = new Client();
		   newClient.setAddressId(7);
		   newClient.setName("test");
		   newClient.setSurname("testSurname");
		   newClient.setUserId(0);
		   clientDao.createClient(newClient);		  		   
	}
	
	private static void jdbcPrintClients(ClientDao clientDao){
		List<Client> clients = clientDao.getAllClients();
		System.out.println(clients.toString());		
	}
}
