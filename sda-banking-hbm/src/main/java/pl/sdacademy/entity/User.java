package pl.sdacademy.entity;

import java.util.LinkedList;
import java.util.List;

public class User {
	private Integer id;
	private String password;
	private boolean enabled;
	private List<UserRole> roles = new LinkedList<UserRole>();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public List<UserRole> getRoles() {
		return roles;
	}
	
	public void setRoles(List<UserRole> roles) {
		this.roles = roles;
	}
	
	
}
