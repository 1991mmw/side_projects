package pl.sdacademy;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import pl.sdacademy.entity.User;
import pl.sdacademy.entity.UserRole;

public class HbmTest {
	
	private static SessionFactory sessionFactory;
	
	public static void main(String[] args) throws Exception {
		initHibernate();
		Session session = sessionFactory.openSession();	
		session.beginTransaction();
		
		User newUser = new User();
		newUser.setPassword("13455");
		newUser.setEnabled(true);
		
		UserRole newUserRole = new UserRole();
		newUserRole.setRole("test1");
		newUserRole.setUser(newUser);
		newUser.getRoles().add(newUserRole);
		
		
		newUserRole = new UserRole();
		newUserRole.setRole("test2");
		newUserRole.setUser(newUser);
		newUser.getRoles().add(newUserRole);
		
		
		session.save(newUser);
		
		
		
		session.getTransaction().commit();	
		session.close();
//
//		session = sessionFactory.openSession();
//		User user = session.find(User.class, Integer.valueOf(6));
//
//		session.close();
//
//		List<UserRole> roles = user.getRoles();
//		System.out.println(roles.size());
		//session.getReference(arg0, arg1)
		
		stopHibernate();
	}
	
	
	private static void initHibernate() throws Exception {
	    // A SessionFactory is set up once for an application
	    sessionFactory = new Configuration()
	            .configure() // configures settings from hibernate.cfg.xml
	            .buildSessionFactory();
	}
	
	
	private static void stopHibernate() throws Exception {
	    // A SessionFactory is set up once for an application
	    sessionFactory.close();
	}
}
