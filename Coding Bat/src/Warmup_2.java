
public class Warmup_2 {

	/*
	 * stringTimes
	 * 
	 * Given a string and a non-negative int n, return a larger string that is n
	 * copies of the original string.
	 * 
	 * stringTimes("Hi", 2) → "HiHi" stringTimes("Hi", 3) → "HiHiHi"
	 * stringTimes("Hi", 1) → "Hi"
	 */

	public String stringTimes(String str, int n) {
		String repeated = repeat(str, n);
		return repeated;
	}

	public String repeat(String str, int n) {
		String newStr = "";
		for (int i = 0; i < n; i++) {
			newStr += str;
		}
		return newStr;
	}

	/*
	 * frontTimes
	 * 
	 * Given a string and a non-negative int n, we'll say that the front of the
	 * string is the first 3 chars, or whatever is there if the string is less
	 * than length 3. Return n copies of the front;
	 * 
	 * frontTimes("Chocolate", 2) → "ChoCho" frontTimes("Chocolate", 3) →
	 * "ChoChoCho" frontTimes("Abc", 3) → "AbcAbcAbc"
	 */

	public String frontTimes(String str, int n) {
		String newnewStr = "";
		if (str.length() < 3) {
			for (int i = 0; i < n; i++) {
				newnewStr += str;
			}
		} else {
			String newStr = str.substring(0, 3);
			for (int i = 0; i < n; i++) {
				newnewStr += newStr;
			}
		}
		return newnewStr;
	}

	/*
	 * countXX
	 * 
	 * Count the number of "xx" in the given string. We'll say that overlapping
	 * is allowed, so "xxx" contains 2 "xx".
	 * 
	 * countXX("abcxx") → 1 countXX("xxx") → 2 countXX("xxxx") → 3
	 */

	int countXX(String str) {
		int count = 0;
		for (int i = 0; i < str.length() - 1; i++) {
			if (str.substring(i, i + 2).equals("xx")) {
				count++;
			}
		}
		return count;
	}

	/*
	 * doubleX
	 * 
	 * Given a string, return true if the first instance of "x" in the string is
	 * immediately followed by another "x".
	 * 
	 * doubleX("axxbb") → true doubleX("axaxax") → false doubleX("xxxxx") → true
	 */

	boolean doubleX(String str) {
		char c = 'x';
		boolean xx = false;
		for (int i = 0; i < str.length() - 1; i++) {
			if (str.charAt(i) == c && str.charAt(i + 1) == c) {
				xx = true;
				break;
			} else if (str.charAt(i) == c && str.charAt(i + 1) != c) {
				xx = false;
				break;
			} else {
				xx = false;
				continue;
			}
		}
		return xx;
	}

	/*
	 * stringBits
	 * 
	 * Given a string, return a new string made of every other char starting
	 * with the first, so "Hello" yields "Hlo".
	 * 
	 * stringBits("Hello") → "Hlo" stringBits("Hi") → "H"
	 * stringBits("Heeololeo") → "Hello"
	 */

	public String stringBits(String str) {
		String newstr = "";
		for (int i = 0; i < str.length(); i++) {
			if (i % 2 == 0) {
				newstr += str.charAt(i);
			}
		}
		return newstr;
	}

	/*
	 * stringSplosion
	 * 
	 * Given a non-empty string like "Code" return a string like "CCoCodCode".
	 * 
	 * stringSplosion("Code") → "CCoCodCode" stringSplosion("abc") → "aababc"
	 * stringSplosion("ab") → "aab"
	 */

	public String stringSplosion(String str) {
		String newstr = "";
		for (int i = 0; i < str.length() + 1; i++) {
			newstr += str.substring(0, i);
		}
		return newstr;
	}

	/*
	 * last2
	 * 
	 * Given a string, return the count of the number of times that a substring
	 * length 2 appears in the string and also as the last 2 chars of the
	 * string, so "hixxxhi" yields 1 (we won't count the end substring).
	 * 
	 * last2("hixxhi") → 1 last2("xaxxaxaxx") → 1 last2("axxxaaxx") → 2
	 */

	public int last2(String str) {
		int count = 0;

		if (str.length() <= 2) {
			return 0;
		} else {
			String endstr = str.substring(str.length() - 2);
			String newstr = str.substring(0, 2);
			count = 0;
			for (int i = 0; i < str.length() - 2; i++) {
				if (str.substring(i, i + 2).equals(endstr)) {
					count++;
				}
			}
		}
		return count;
	}

	/*
	 * arrayCount9
	 * 
	 * Given an array of ints, return the number of 9's in the array.
	 * 
	 * arrayCount9([1, 2, 9]) → 1 arrayCount9([1, 9, 9]) → 2 arrayCount9([1, 9,
	 * 9, 3, 9]) → 3
	 */

	public int arrayCount9(int[] nums) {
		int count = 0;
		for (int i : nums) {
			if (i == 9) {
				count++;
			}
		}
		return count;
	}

	/*
	 * arrayFront9
	 * 
	 * Given an array of ints, return true if one of the first 4 elements in the
	 * array is a 9. The array length may be less than 4.
	 * 
	 * arrayFront9([1, 2, 9, 3, 4]) → true arrayFront9([1, 2, 3, 4, 9]) → false
	 * arrayFront9([1, 2, 3, 4, 5]) → false
	 */

	public boolean arrayFront9(int[] nums) {
		boolean nine = false;
		if (nums.length < 3) {
			for (int i = 0; i < nums.length; i++) {
				if (nums[i] == 9) {
					nine = true;
				}
			}
		} else {
			for (int i = 0; i < nums.length - 1; i++) {
				if (nums[i] == 9) {
					nine = true;
				}
			}
		}
		return nine;
	}

	/*
	 * array123
	 * 
	 * Given an array of ints, return true if the sequence of numbers 1, 2, 3
	 * appears in the array somewhere.
	 * 
	 * array123([1, 1, 2, 3, 1]) → true array123([1, 1, 2, 4, 1]) → false
	 * array123([1, 1, 2, 1, 2, 3]) → true
	 */

	public boolean array123(int[] nums) {
		boolean _123 = false;
		boolean part1 = false;
		boolean part2 = false;
		boolean part3 = false;
		if (nums.length < 3) {
			_123 = false;
		} else {
			for (int i = 0; i < nums.length; i++) {
				if (nums[i] == 1) {
					part1 = true;
				}
				if (nums[i] == 2 && part1) {
					part2 = true;
				}
				if (nums[i] == 3 && part1 && part2) {
					part3 = true;
				}
			}
			if (part1 && part2 && part3) {
				_123 = true;
			}
		}
		return _123;
	}

	/*
	 * stringMatch
	 * 
	 * Given 2 strings, a and b, return the number of the positions where they
	 * contain the same length 2 substring. So "xxcaazz" and "xxbaaz" yields 3,
	 * since the "xx", "aa", and "az" substrings appear in the same place in
	 * both strings.
	 * 
	 * stringMatch("xxcaazz", "xxbaaz") → 3 stringMatch("abc", "abc") → 2
	 * stringMatch("abc", "axc") → 0
	 */

	public int stringMatch(String a, String b) {
		String str1 = a;
		String str2 = b;
		int count = 0;

		for (int i = 0; i < str1.length() - 1; i++) {
			String s1 = (String) str1.subSequence(i, i + 2);
			for (int j = 0; j < str2.length() - 1; j++) {
				String s2 = (String) str2.subSequence(j, j + 2);
				if (s1.equals(s2) && j == i) {
					count++;
				}
			}
		}
		return count;
	}

	/*
	 * stringX
	 * 
	 * Given a string, return a version where all the "x" have been removed.
	 * Except an "x" at the very start or end should not be removed.
	 * 
	 * stringX("xxHxix") → "xHix" stringX("abxxxcd") → "abcd"
	 * stringX("xabxxxcdx") → "xabcdx"
	 */

	public String stringX(String str) {
		String x = "x";
		String _temp = "";
		if (str.length() < 3) {
			return str;
		}
		if (str.substring(0, 1).equals(x)) {
			for (int i = 0; i < str.length(); i++) {
				if (str.substring(i, i + 1).equals(x)) {

				} else {
					_temp += str.substring(i, i + 1);
				}
			}
			return "x" + _temp + "x";
		} else {
			for (int i = 0; i < str.length(); i++) {
				if (str.substring(i, i + 1).equals(x)) {

				} else {
					_temp += str.substring(i, i + 1);
				}
			}
			return _temp;
		}
	}

	/*
	 * altPairs
	 * 
	 * Given a string, return a string made of the chars at indexes 0,1, 4,5,
	 * 8,9 ... so "kittens" yields "kien".
	 * 
	 * altPairs("kitten") → "kien" altPairs("Chocolate") → "Chole"
	 * altPairs("CodingHorror") → "Congrr"
	 */

	public String altPairs(String str) {
		String temp = "";
		for (int i = 0; i < str.length(); i++) {
			switch (i) {
			case 0:
				temp += str.substring(0, 1);
				break;
			case 1:
				temp += str.substring(1, 2);
				break;
			case 4:
				temp += str.substring(4, 5);
				break;
			case 5:
				temp += str.substring(5, 6);
				break;
			case 8:
				temp += str.substring(8, 9);
				break;
			case 9:
				temp += str.substring(9, 10);
				break;
			case 12:
				temp += str.substring(12, 13);
				break;
			case 13:
				temp += str.substring(13, 14);
				break;
			default:
				break;
			}
		}
		return temp;
	}

	/*
	 * stringYak
	 * 
	 * Suppose the string "yak" is unlucky. Given a string, return a version
	 * where all the "yak" are removed, but the "a" can be any char. The "yak"
	 * strings will not overlap.
	 * 
	 * stringYak("yakpak") → "pak" stringYak("pakyak") → "pak"
	 * stringYak("yak123ya") → "123ya"
	 */

	public String stringYak(String str) {
		String unlucky = "yak";
		String temp = "";

		if (str.contains(unlucky)) {
			temp = str.replace(unlucky, "");
		}
		return temp;
	}

	/*
	 * array667
	 * 
	 * Given an array of ints, return the number of times that two 6's are next
	 * to each other in the array. Also count instances where the second "6" is
	 * actually a 7.
	 * 
	 * array667([6, 6, 2]) → 1 array667([6, 6, 2, 6]) → 1 array667([6, 7, 2, 6])
	 * → 1
	 */

	public int array667(int[] nums) {
		int count = 0;
		for (int i = 0; i < nums.length - 1; i++) {
			if (nums[i] == 6) {
				if (nums[i + 1] == 6 || nums[i + 1] == 7) {
					count++;
				}
			}
		}
		return count;
	}

	/*
	 * noTriples
	 * 
	 * Given an array of ints, we'll say that a triple is a value appearing 3
	 * times in a row in the array. Return true if the array does not contain
	 * any triples.
	 * 
	 * noTriples([1, 1, 2, 2, 1]) → true noTriples([1, 1, 2, 2, 2, 1]) → false
	 * noTriples([1, 1, 1, 2, 2, 2, 1]) → false
	 */

	public boolean noTriples(int[] nums) {
		for (int i = 0; i < (nums.length - 2); i++) {
			if (nums[i] == nums[i + 1] && nums[i + 1] == nums[i + 2]) {
				return false;
			}
			continue;
		}
		return true;
	}

}
