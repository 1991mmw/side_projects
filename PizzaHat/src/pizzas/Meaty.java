package pizzas;

public class Meaty extends Pizza{
	public Meaty() {
		name = "Meaty";
		price = 20;
		dough.add("Americano");
		meat.add("Beef");
		meat.add("Ham");
		
	}

	@Override
	public String toString() {
		return "Meaty [name=" + name + ", price=" + price + ", dough=" + dough + ", meat=" + meat + "]";
	}
	
}
