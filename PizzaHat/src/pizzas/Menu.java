package pizzas;

import java.util.Scanner;

public class Menu {
	
	Scanner scanner = new Scanner(System.in);
	
	public void DisplayMenu(){
		
		Margharita margharita = new Margharita();
		Meaty meaty = new Meaty();
		Vegitarian vegitarian = new Vegitarian();
		
		System.out.println(margharita);
		System.out.println(meaty);
		System.out.println(vegitarian);
		System.out.println();
		System.out.println("Welcome to PizzaHat, choose a pizza.");
		choosePizza();
	}
	
	public void choosePizza() {
		String pizzaChoice = scanner.nextLine();
		if(pizzaChoice.equalsIgnoreCase("margharita")||pizzaChoice.equalsIgnoreCase("meaty")||pizzaChoice.equalsIgnoreCase("vegitarian")){
			chooseTopping(pizzaChoice);
			
		}else{
			System.out.println("We don't have this pizza in stock! Please choose again.");
			choosePizza();
		}
		
	}

	public void chooseTopping(String pizzaChoice) {
		System.out.println("You have chosen " + pizzaChoice + ". Would you like additional toppings?");
		System.out.println("                            YES || NO"                              );
		String toppingChoice = scanner.nextLine();
		if(toppingChoice.equalsIgnoreCase("yes")||toppingChoice.equalsIgnoreCase("no")){
			if(toppingChoice.equalsIgnoreCase("yes")){
				addToppings();
			}else{
				
			}
		}else{
			System.out.println("Invalid Choice. Would you like additional toppings?");
			choosePizza();
		}
	}

	public void addToppings() {
		extraCheese extraCheese = new extraCheese();
		onion onion = new onion();
		brocolli brocolli = new brocolli();
		chicken chicken = new chicken();
		beef beef = new beef();
		System.out.println(extraCheese);
		System.out.println(onion);
		System.out.println(brocolli);
		System.out.println(chicken);
		System.out.println(beef);
		System.out.println();
		System.out.println("Please choose a topping");
		
	}

	public static void main(String[] args) {
		Menu menu = new Menu();
		menu.DisplayMenu();
	}
}
