package pizzas;

public class Margharita extends Pizza {

	public Margharita() {
		name = "Margharita";
		price = 15;
		dough.add("Italiano");
	}

	@Override
	public String toString() {
		return "Margharita [name=" + name + ", price=" + price + ", dough=" + dough + "]";
	}
	
	
	
	

}
