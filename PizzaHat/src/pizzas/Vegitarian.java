package pizzas;

public class Vegitarian extends Pizza{
	public Vegitarian() {
		name = "Vegitarian";
		price = 17;
		dough.add("Italiano");
		veg.add("Brocolli");
		veg.add("Peppers");
	}

	@Override
	public String toString() {
		return "Vegitarian [name=" + name + ", price=" + price + ", dough=" + dough + ", veg=" + veg + "]";
	}
	
}
