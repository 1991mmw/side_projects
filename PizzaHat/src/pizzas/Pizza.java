package pizzas;

import java.util.ArrayList;

public abstract class Pizza {
	
	String name;
	double price;
	ArrayList<String> dough = new ArrayList<String>();
	ArrayList<String> meat = new ArrayList<String>();
	ArrayList<String> veg = new ArrayList<String>();
	boolean markToppings = false;
	
	public ArrayList<String> addToppings(String choice, String vegChoice, String meatChoice) {
		ArrayList<String> addToppings = new ArrayList<String>();
		switch (choice) {
		case "addVeg":
			switch (vegChoice) {
			case "Brocolli":
				veg.add("Brocolli");
				break;
			case "Onion":
				veg.add("Onion");
				break;
			}
			break;
		case "addMeat":
			switch (meatChoice) {
			case "Beef":
				meat.add("Beef");
				break;
			case "Chicken":
				meat.add("Chicken");
				break;
			}
			break;
		}
		return addToppings;
	}
	
}
