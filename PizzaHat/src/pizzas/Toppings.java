package pizzas;

public abstract class Toppings{
	
	String name;
	double price;
	
}
class extraCheese extends Toppings{
	public extraCheese() {
		name = "Extra Cheese";
		price = 1.49;
	}

	@Override
	public String toString() {
		return "extraCheese [name=" + name + ", price=" + price + "]";
	}
	
}
class onion extends Toppings{
	public onion() {
		name = "Onion";
		price = 1.99;
	}

	@Override
	public String toString() {
		return "onion [name=" + name + ", price=" + price + "]";
	}
	
}
class brocolli extends Toppings{
	public brocolli() {
		name = "Brocolli";
		price = 1.99;
	}

	@Override
	public String toString() {
		return "brocolli [name=" + name + ", price=" + price + "]";
	}
	
}
class chicken extends Toppings{
	public chicken() {
		name = "Chicken";
		price = 4.99;
	}

	@Override
	public String toString() {
		return "chicken [name=" + name + ", price=" + price + "]";
	}
	
}
class beef extends Toppings{
	public beef() {
		name = "Beef";
		price = 5.99;
	}

	@Override
	public String toString() {
		return "beef [name=" + name + ", price=" + price + "]";
	}
	
}