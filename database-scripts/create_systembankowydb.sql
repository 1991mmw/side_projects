-- -----------------------------------------------------
-- Schema systembankowydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `systembankowydb` DEFAULT CHARACTER SET utf8 ;
USE `systembankowydb` ;

-- -----------------------------------------------------
-- Table `systembankowydb`.`address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `systembankowydb`.`address` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `city` VARCHAR(45) NULL DEFAULT NULL,
  `street` VARCHAR(45) NULL DEFAULT NULL,
  `number` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `systembankowydb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `systembankowydb`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `password` VARCHAR(60) NOT NULL,
  `enabled` TINYINT(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `systembankowydb`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `systembankowydb`.`client` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(100) NOT NULL,
  `address_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `address_id` (`address_id` ASC),
  INDEX `fk_user_client` (`user_id` ASC),
  CONSTRAINT `fk_address_client`
    FOREIGN KEY (`address_id`)
    REFERENCES `systembankowydb`.`address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_client`
    FOREIGN KEY (`user_id`)
    REFERENCES `systembankowydb`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `systembankowydb`.`client_account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `systembankowydb`.`client_account` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `client_id` INT(11) NOT NULL,
  `number` VARCHAR(15) NOT NULL,
  `amount` DOUBLE NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `client_account_ibfk_1` (`client_id` ASC),
  CONSTRAINT `client_account_ibfk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `systembankowydb`.`client` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `systembankowydb`.`history_account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `systembankowydb`.`history_account` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `account_id` INT(11) NOT NULL,
  `number` VARCHAR(15) NOT NULL,
  `amount` DOUBLE NOT NULL DEFAULT '0',
  `transfer_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `history_account_ibfk_1` (`account_id` ASC),
  CONSTRAINT `history_account_client_account`
    FOREIGN KEY (`account_id`)
    REFERENCES `systembankowydb`.`client_account` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `systembankowydb`.`user_roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `systembankowydb`.`user_roles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `ROLE` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_role` (`user_id` ASC),
  CONSTRAINT `fk_user_role`
    FOREIGN KEY (`user_id`)
    REFERENCES `systembankowydb`.`user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;
